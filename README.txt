MuMu is a simple module that allows you to legally (read: Drupally) upload a single CSS and a single JS file to your website.

Why In The World Would You Need To Do This?

Good question. Sometimes it is overkill to create a subtheme just to add several CSS or JS changes / additions. As great Drupal themes are now being created (and with the addition of awesome modules like DisplaySuite), one can build and configure an awesome Drupal site without the need to extend a Base theme.

If you have configured your site to use an awesome theme (like Omega) but only want to make CSS changes and add a couple of JS enhancements, this module is perfect for you.

If your CSS extends beyond 1000 lines, you should consider extending a base theme. The same rule applies if you are adding JS as well as adding libraries.

How to Use

1. Download and install the module on your server like any other Drupal Module.
2. Enable the module in /admin/module
3. Navigate /admin/appearance
4. Look for the "MUMU" tab
5. Upload your files